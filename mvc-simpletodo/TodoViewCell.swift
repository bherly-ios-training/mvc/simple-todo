//
//  TodoViewCell.swift
//  mvc-simpletodo
//
//  Created by Bherly Novrandy on 1/29/18.
//  Copyright © 2018 Bherly Novrandy. All rights reserved.
//

import UIKit

class TodoViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var doneImage: UIImageView!
    
    var todoModel: TodoModel? {
        didSet {
            if let todo = todoModel {
                self.nameLabel.text = todo.name
                self.doneImage.image = todo.done ? UIImage(named: "icon-done") : UIImage(named: "icon-undone")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
