//
//  TodoProtocol.swift
//  mvc-simpletodo
//
//  Created by Bherly Novrandy on 1/29/18.
//  Copyright © 2018 Bherly Novrandy. All rights reserved.
//

import Foundation

protocol TodoDelegate: class {
    func didAddTodo(todo: TodoModel)
    func didUpdateTodo(todo: TodoModel)
}

