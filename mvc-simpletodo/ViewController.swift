//
//  ViewController.swift
//  mvc-simpletodo
//
//  Created by Bherly Novrandy on 1/24/18.
//  Copyright © 2018 Bherly Novrandy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myButton: UIButton!
    var dataSource: TodoDataSourceProtocol = TodoDataSource()
    
    weak var delegate: TodoDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.delegate = self
        
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "tableViewCell")
        myTableView.register(UINib(nibName: "TodoViewCell", bundle:nil), forCellReuseIdentifier: "todoViewCell")
        reloadTableView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func myButtonTap(_ sender: UIButton) {
        gotoTodoPage(todo: nil)
    }
    
    func reloadTableView() {
        myTableView.reloadData()
    }
    
    func gotoTodoPage(todo: TodoModel?) {
        if let todoViewController = self.storyboard?.instantiateViewController(withIdentifier: "TodoViewController") as? TodoViewController {
            todoViewController.delegate = self.delegate
            todoViewController.todoModel = todo
            self.present(todoViewController, animated: true, completion: nil)
        }
    }
}

extension ViewController: TodoDelegate {
    func didAddTodo(todo: TodoModel) {
        dataSource.addTodo(todo: todo)
        reloadTableView()
    }
    
    func didUpdateTodo(todo: TodoModel) {
        dataSource.updateTodo(todo: todo)
        reloadTableView()
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataSource.todos.count == 0 {
            return 1
        }
        return dataSource.todos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if dataSource.todos.count == 0 {
            let cell: UITableViewCell = UITableViewCell(style: .default, reuseIdentifier: "tableViewCell")
            cell.textLabel?.text = "No Todo found"
            cell.textLabel?.textAlignment = .center
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "todoViewCell", for: indexPath) as! TodoViewCell
            if let todo = dataSource.get(at: indexPath.row) {
                cell.todoModel = todo
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let todo = dataSource.get(at: indexPath.row) {
            gotoTodoPage(todo: todo)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if dataSource.todos.count == 0 {
            return false
        } else {
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        var title: String = ""
        var todoModel: TodoModel?
        var doneColor: UIColor = UIColor.green
        if let todo = dataSource.get(at: indexPath.row) {
            title = todo.done ? "Undone" : "Done"
            doneColor = todo.done ? UIColor.lightGray : UIColor.green
            todoModel = todo
        }
        
        let doneRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: title, handler:{action, indexpath in
            if let todo = todoModel {
                todo.done = !todo.done
                self.dataSource.updateTodo(todo: todo)
                self.reloadTableView()
            }
        });
        doneRowAction.backgroundColor = doneColor
        
        let deleteRowAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Delete", handler:{action, indexpath in
            self.dataSource.removeTodo(at: indexPath.row)
            self.reloadTableView()
        });
        
        return [deleteRowAction, doneRowAction];
    }

}
