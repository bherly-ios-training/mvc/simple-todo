//
//  UserModel.swift
//  mvc-simpletodo
//
//  Created by Bherly Novrandy on 1/24/18.
//  Copyright © 2018 Bherly Novrandy. All rights reserved.
//

import Foundation

class TodoModel {
    var id: Int
    var name: String
    var description: String
    var done: Bool
    var timestamp: Double
    
    init(id: Int = 0, name: String, description: String, done: Bool = false, timestamp: Double = Date().timeIntervalSince1970) {
        self.id = id
        self.name = name
        self.description = description
        self.done = done
        self.timestamp = timestamp
    }
}
