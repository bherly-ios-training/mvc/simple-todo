//
//  TodoViewController.swift
//  mvc-simpletodo
//
//  Created by Bherly Novrandy on 1/28/18.
//  Copyright © 2018 Bherly Novrandy. All rights reserved.
//

import Foundation
import UIKit

class TodoViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var descTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var timeTextField: UITextField!
    
    var delegate: TodoDelegate?
    var todoModel: TodoModel?
    let datePicker: UIDatePicker = UIDatePicker()
    let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
        formatter.dateFormat = "dd/MM/yyyy"
        if let todo = todoModel {
            loadExistingModel(todo: todo)
        }
        
        self.timeTextField.delegate = self
        self.timeTextField.addTarget(self, action: #selector(timeTextFieldTap), for: UIControlEvents.touchDown)

    }
    
    func loadExistingModel(todo: TodoModel) {
        self.nameTextField.text = todo.name
        self.descTextField.text = todo.description
        self.timeTextField.text = formatDateToString(date: Date(timeIntervalSince1970: todo.timestamp))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.timeTextField {
            textField.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    @IBAction func saveButtonTap(_ sender: UIButton) {
        if let name = self.nameTextField.text, let desc = self.descTextField.text, let time = self.timeTextField.text, !name.isEmpty {
            if let todo = todoModel {
                todo.name = name
                todo.description = desc
                if let epochTime = dateStringToEpoch(dateString: time) {
                    todo.timestamp = epochTime
                }
                self.delegate?.didUpdateTodo(todo: todo)
            } else {
                let todo: TodoModel = TodoModel(name: name, description: desc)
                self.delegate?.didAddTodo(todo: todo)
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func timeTextFieldTap() {
        showDatePicker()
    }
    
    func showDatePicker() {
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.bordered, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.bordered, target: self, action: #selector(cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        timeTextField.inputAccessoryView = toolbar
        timeTextField.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        timeTextField.text = formatDateToString(date: datePicker.date)
        cancelDatePicker()
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func formatDateToString(date: Date) -> String {
        return formatter.string(from: date)
    }
    
    func dateStringToEpoch(dateString: String) -> Double? {
        if let date = formatter.date(from: dateString) {
            return date.timeIntervalSince1970
        }
        
        return nil
    }
}
