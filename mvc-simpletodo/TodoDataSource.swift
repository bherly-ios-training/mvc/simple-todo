//
//  TodoDataSource.swift
//  mvc-simpletodo
//
//  Created by Bherly Novrandy on 1/29/18.
//  Copyright © 2018 Bherly Novrandy. All rights reserved.
//

import Foundation

protocol TodoDataSourceProtocol {
    var todos: [TodoModel] { get set }
    
    func addTodo(todo: TodoModel)
    func updateTodo(todo: TodoModel)
    func removeTodo(at: Int)
    func get(at: Int) -> TodoModel?
    func clearTodo()
}

class TodoDataSource: TodoDataSourceProtocol {
    var todos: [TodoModel] = []
    
    func addTodo(todo: TodoModel) {
        todo.id = todos.count + 1
        todos.append(todo)
    }
    
    func updateTodo(todo: TodoModel) {
        if let indeks = searchTodo(id: todo.id) {
            todos[indeks] = todo
        }
    }
    
    private func searchTodo(id: Int) -> Int? {
        for (index,todo) in todos.enumerated() {
            if todo.id == id {
                return index
            }
        }
        
        return nil
    }
    
    func removeTodo(at: Int) {
        todos.remove(at: at)
    }
    
    func clearTodo() {
        todos.removeAll()
    }
    
    func get(at: Int) -> TodoModel? {
        if todos.count == 0 || at > todos.count  - 1{
            return nil
        }
        return todos[at]
    }
}
